const connection = require('./connection/connection');
const io = require('./connection/connection');
ioConnection = io.ioConnection();

ioConnection.on('connection', (socket) => {
    console.log(`Usuario conectado en socket: ${socket.id}`);
    socket.emit('conectado', { user: socket.id });
});