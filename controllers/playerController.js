const createplayer = require('../models/player');

exports.postNewUser = (req, res) => {
    var user = JSON.parse(req.body.user)
    createplayer.findOne({ uid: user.uid }, { new: true }, function (err, result) {
        if (!result) {
            console.log("user")
            console.log(user)
            let newUser = new createplayer();
            Object.assign(newUser, user);
            newUser.save()
                .then(() => {
                    res.send({ newUser });
                })
                .catch(err => console.error('newUser post error: ', err))
        }
        if (result) {
            console.log("user exist")
            console.log("result " + user)
            console.log(user.displayName.toLowerCase())
            res.send("Welcome: " + user.displayName.toLowerCase());
        }
        if (err) {
            return res.send(`Server error ${err}`);
        }
    })
}