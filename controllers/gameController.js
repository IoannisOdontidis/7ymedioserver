const Game = require('../models/game');

// exports.postGameById = (req, res) => {
//     //let seriesId = req.params.id;
//     let newGame = new Game();
//     Object.assign(newGame, req.body);
//     newGame.save()
//         .then((newGame) => {
//             console.log({ newGame: newGame });
//             res.send({ newGame: newGame });
//         })
//         .catch(err => console.error('La partida no ha podido guardarse: ', err));
// };
// exports.getAllGames = (req, res) => {
//     Game.find({}, (err, game) => {
//         if (err) {
//             console.log(`Error al realizar la petición: ${err}`);
//             return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
//         }
//         if (!game) {
//             console.log(`No existe series ${series}`);
//             return res.status(404).send({ message: `No existen series` });
//         }
//         console.log({ game: game });
//         res.render('index.hbs', { game });
//     });
// };
exports.getGameId = (req, res) => {
    let GameId = req.params.id;
    Game.find({ _id: GameId }, (err, game) => {
        if (err) {
            console.log(`Error al realizar la petición: ${err}`);
            return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
        }
        if (!game) {
            console.log(`No existe esa id ${game._id}`);
            return res.status(404).send({ message: `No existe esa id` });
        }
        console.log({ game: game });
        res.send({ game: game });

    });
};
exports.getGameByCode = (req, res) => {
    let Gamecode = req.params.code;
    Game.find({ code: Gamecode }, (err, game) => {
        if (err) {
            console.log(`Error al realizar la petición: ${err}`);
            return res.status(500).send({ message: `Error al realizar la petición: ${err}` });
        }
        if (!game) {
            console.log(`No existe esa codigo ${Gamecode}`);
            return res.status(404).send({ message: `No existe una partida con ese codigo` });
        }
        res.send({ game: game });
        console.log({ game: game });
    })
};
exports.postGame = (req, res) => {
    //let seriesId = req.params.id;
    let newGame = new Game();
    let code = Date.now();
    req.body.code = code;
    Object.assign(newGame, req.body);
    newGame.save()
        .then((newGame) => {
            console.log({ newGame: newGame });
            res.send({ newGame });
        })
        .catch(err => console.error('La partida no ha podido guardarse: ', err));
};