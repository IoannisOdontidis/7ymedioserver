const express = require('express');
const router = express.Router();
const gameController = require('../controllers/gameController');
const playerController = require('../controllers/playerController');
const validator = require('../validator/Validator');

//Instalar validador


//Routes
//PLAYER
router.post('/newUser',playerController.postNewUser);
// router.get('/game', gameController.getAllGames);

//GAME
router.get('/game/:id', gameController.getGameId);
router.get('/gamecode/:code', gameController.getGameByCode);
router.post('/gameinsert', gameController.postGame);

module.exports = router;