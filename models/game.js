const mongoose = require('mongoose'); //Para crear modelos lo necesitamos
const Schema = mongoose.Schema;  
const playerModel = require('./player');

//Creamos el Schema
const gameSchema = new mongoose.Schema({
     
     code: Number,     
     players: [{type:Schema.ObjectId, ref: "playerModel"}],
})


//Hay que exportarlo para poder usarlo desde cualquier otro fichero
module.exports = mongoose.model('game', gameSchema)