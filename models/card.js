const mongoose = require('mongoose'); //Para crear modelos lo necesitamos



//Creamos el Schema
const cardSchema = new mongoose.Schema({
     
     number: Number,
     value: Number,
     type: String    

})

//Hay que exportarlo para poder usarlo desde cualquier otro fichero
module.exports = mongoose.model('card', cardSchema)