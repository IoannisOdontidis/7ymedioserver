const mongoose = require('mongoose');

const playerSchema = new mongoose.Schema({
    
    uid: String,
    name:  String,
    displayName: String,
    email: String,
    photo: {type: String, default: null}
});
module.exports = mongoose.model('player', playerSchema);