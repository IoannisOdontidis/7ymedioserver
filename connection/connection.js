const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const port = process.env.PORT || 3002;
const app = express();
const server = http.createServer(app);
const io = socketIO(server, { pingTimeout: 30000 });
//Route mongo
const mongoose =  require ('mongoose')
const bodyParser= require ('body-parser')
const mongodbRoute = 'mongodb://ioannis1:ioannis1@ds063789.mlab.com:63789/ioannis'
const router = require('../routes/Router');

const expessHbs = require('express-handlebars');
const hbs = require('hbs');


/*app.use(express.static(__dirname + '/public'));
app.engine( 'hbs', expessHbs( { 
  extname: 'hbs', 
  defaultLayout: 'index', 
  layoutsDir: __dirname + '/views/',
  partialsDir: __dirname + '/views/partials/'
} ) ); */

app.use (bodyParser.urlencoded({ extended: false}));
app.use (bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})

app.use(router);

/*MONGODB*/
const options = {
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30,
  useNewUrlParser: true
};
mongoose.Promise = global.Promise
mongoose.connect(mongodbRoute, options, (err) => {
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    server.listen(port, () => {
		console.log(`Servidor up en ${port}`);
	});
    console.log(`Conexión con Mongo correcta.`)
})

exports.ioConnection = () => {
  return io;
};
